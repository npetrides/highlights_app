import ExpoFastImage from 'expo-fast-image';
import React from "react";
import { TouchableWithoutFeedback, View } from "react-native";
import styles from "../config/styles";

function TeamIcon({ imageUrl, onPress, uniqueId }) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View>
        <ExpoFastImage
          uri={imageUrl}
          cacheKey={uniqueId}
          style={styles.teamIcon.imageContainer}
        />
      </View>
    </TouchableWithoutFeedback>
  );
}

export default TeamIcon;
