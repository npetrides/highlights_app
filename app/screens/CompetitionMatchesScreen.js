import React, { useState, useEffect } from "react";
import { ActivityIndicator, FlatList, SafeAreaView, RefreshControl, View } from "react-native";
import ListItem from "../components/ListItem";
import colors from "../config/colors";
import styles from "../config/styles";
import routes from "../navigation/routes";
import Text from "../components/Text";
import { useQuery } from '@apollo/client';
import CompetitionMatchesQuery from "../api/competitionMatchesQuery";
import moment from "moment";
import * as storage from "../api/asyncStorage";
import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import HeartIcon from "../components/HeartIcon";
import { KEYS } from "../utility/Contants";
import { ShowToast } from "../utility/Helper";

let isCheck = false;
const PAGE_SIZE = 30;

function CompetitionMatchesScreen({ navigation, route }) {
  const { competitionName } = route.params;
  const { competitionId } = route.params;

  const [value, setValue] = useState('value');
  const { getItem, setItem } = useAsyncStorage(KEYS.favoriteLeagues);
  const [isAdded, setIsAdded] = useState(false);
  const [page, setPage] = useState(1);
  const [matches, setMatches] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    readItemFromStorage({});
  }, [competitionId]);

  const readItemFromStorage = async () => {
    const item = await getItem();
    checkItem(item, competitionId);
    setValue(item);
  };

  const favoritesToggle = (name, id, key) => {
    if (isAdded) {
      storage.removeFavorite(id, key);
      ShowToast("Removed " + name + " from favorites");
      setIsAdded(false);
    } else {
      storage.addFavorite(id, key);
      ShowToast("Added " + name + " to favorites");
      setIsAdded(true);
    }
  };

  const checkItem = (value, id) => {
    if (value) {
      if (value.includes(id)) {
        setIsAdded(true)
      } else {
        setIsAdded(false)
      }
    }
  };

  const MatchesQuery = CompetitionMatchesQuery(competitionId);
  const { loading, error, data } = useQuery(MatchesQuery, {
    variables: {
      skip: page === 1 ? 0 : page * PAGE_SIZE,
      limit: PAGE_SIZE,
    }
  });

  useEffect(() => {
    data && setMatches([...matches, ...data.recentMatchCollection.items])
  }, []);

  const getMoreData = () => {
    setIsLoading(true);
    setTimeout(() => {
      setPage(prev => prev + 1)
      setIsLoading(false);
    }, 2000);
  };

  if (loading && !isCheck) return isCheck = true;
  if (error) return <></>;

  const items = data && data.recentMatchCollection.items

  return (
    <SafeAreaView>
      <FlatList
        style={styles.container}
        onEndReachedThreshold={0.5}
        onEndReached={getMoreData}
        ListHeaderComponent={
          <View style={styles.teamNameWithHeartIcon}>
            <Text style={styles.titleWithNav}>
              {competitionName}
            </Text>
            <HeartIcon
              onPress={() => favoritesToggle(competitionName, competitionId, KEYS.favoriteLeagues)}
              color={isAdded ? colors.textHighlight : colors.offBackground}
            />
          </View>
        }
        ListFooterComponent={
          <View style={styles.spinnerView}>
            {isLoading && <ActivityIndicator size="large" color={colors.text} />}
          </View>
        }
        data={matches.length === 0 ? items : matches}
        keyExtractor={(item, index) => String(item.sys.id + index)}
        renderItem={({ item }) => (
          <ListItem
            uniqueId={item.title.replace(/[^\w]/g, "") + `${item.homeSide.sys.id.toString()}${item.awaySide.sys.id.toString()}`}
            title={item.title}
            date={moment(item.matchDate).fromNow()}
            subTitle={item.competition.name}
            imageUrl={item.primaryVideo.thumbnail}
            thumbnailUrl={item.primaryVideo.thumbnail}
            videoId={item.primaryVideo.youtubeId}
            onPress={() => navigation.navigate(routes.MATCH_DETAILS, item)}
          />
        )}
      />
    </SafeAreaView>
  );
}

export default CompetitionMatchesScreen;