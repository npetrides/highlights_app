import React from "react";
import { View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import colors from "../config/colors";
import AsyncStorage from '@react-native-async-storage/async-storage';

// Prevent "null" or null from being added to storage

export async function addFavorite(id, key) {
  try {
    AsyncStorage.getItem(key, (err, result) => {
      if (!err && result != null) { // found favoriteTeams
        let parsedFavorites = JSON.parse(result);
        parsedFavorites.indexOf(id) === -1 ? parsedFavorites.push(id) : console.log("This item already exists");
        AsyncStorage.setItem(key, JSON.stringify(parsedFavorites));
      }
      else { // no favoriteTeams
        const parsedFavorites = [];
        parsedFavorites.push(id);
        AsyncStorage.setItem(key, JSON.stringify(parsedFavorites));
      }
    });
  }
  catch (error) {
    alert(error);
  }
}

export async function removeFavorite(id, key) {
  try {
    AsyncStorage.getItem(key, (err, result) => {
      if (!err && result != null) { // found favoriteTeams
        let parsedFavorites = JSON.parse(result);
        for (var i = 0; i < parsedFavorites.length; i++) {
          if (parsedFavorites[i] === id) {
            parsedFavorites.splice(i, 1);
          }
        }
        AsyncStorage.setItem(key, JSON.stringify(parsedFavorites));
        return true
      }
      else { // no favoriteTeams
        console.log('No favorites')
      }
    });
  }
  catch (error) {
    alert(error);
  }
}

export async function displayStoredValue(key) {
  try {
    let value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
  }
  catch (error) {
    alert(error)
  }
}



export function favoriteIndicator(favorites, id1, id2) {
  if (favorites != null) {
    if (favorites.includes(id1)) {
      return <View style={{ justifyContent: 'space-between' }}><MaterialCommunityIcons name="heart" size={20} color={colors.textHighlight} /></View>
    }
    if (favorites.includes(id2)) {
      return <View><MaterialCommunityIcons name="heart" size={20} color={colors.textHighlight} /></View>
    }
  }
}


// https://medium.com/wesionary-team/react-native-persist-values-pattern-for-beginners-c74aa1a037bf
// https://www.youtube.com/watch?v=PhhyBmAIehg
// https://stackoverflow.com/questions/58015602/how-to-rerender-component-in-useeffect-hook
// https://github.com/arnnis/react-native-toast-notifications