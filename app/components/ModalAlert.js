import React from 'react';
import { Modal, StyleSheet, Text, View} from 'react-native';
import colors from "../config/colors";

const toggleModal = () => {
  setModalVisible(true);
  setTimeout( () => {
    setModalVisible(false);
  }, 3000)
};

const ModalAlert = ({ title, image, isVisible }) => {

return (
  <View style={styles.modalContainer}>
  <Modal animationType='fade' transparent={true} visible={isVisible}>
      <View style={styles.modalContainer}>
      <View style={styles.modalView}>
        <Text style={styles.textStyle}>{title}</Text>
        </View>
      </View>
    </Modal>
  </View>
  );
  };

  const styles = StyleSheet.create({
  modalContainer: {
    alignItems: 'center',
    position: 'absolute',
    height: '100%', width: '100%',
  },
  modalView: {
    flexDirection: 'row',
    justifyContent: 'flex-start', alignItems: 'center',
    top: 100,
    width: '90%', height: 50,
    backgroundColor: colors.offBackground,
    borderRadius: 8
  },
  textStyle: {
    color: colors.text,
    textAlign: 'center',
    fontSize: 24,
    marginLeft: 20
  }
  });

export default ModalAlert;