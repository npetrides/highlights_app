import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import CountriesScreen from "../screens/CountriesScreen";
import CompetitionsScreen from "../screens/CompetitionsScreen";
import CompetitionMatchesScreen from "../screens/CompetitionMatchesScreen";
import TeamMatchesScreen from "../screens/TeamMatchesScreen";
import MatchDetailsScreen from "../screens/MatchDetailsScreen";
import styles from "../config/styles";

const Stack = createStackNavigator();

const CompetitionsNavigator = () => (
  <Stack.Navigator headerMode="screen" screenOptions={styles.navStyles}>
    <Stack.Screen name="Countries" component={CountriesScreen} options={styles.hideHeader}/>
    <Stack.Screen name="Competitions" component={CompetitionsScreen} options={styles.showHeader}/>
    <Stack.Screen name="CompetitionMatches" component={CompetitionMatchesScreen} options={styles.showHeader}/>
    <Stack.Screen name="TeamMatches" component={TeamMatchesScreen} options={styles.showHeader}/>
    <Stack.Screen name="MatchDetails" component={MatchDetailsScreen} options={styles.showHeader}/>
  </Stack.Navigator>
);

export default CompetitionsNavigator;
