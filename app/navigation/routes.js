export default Object.freeze({
  TEAMS: "Teams",
  COMPETITIONS: "Competitions",
  COMPETITION_MATCHES: "CompetitionMatches",
  TEAM_MATCHES: "TeamMatches",
  MATCH_DETAILS: "MatchDetails",
});
