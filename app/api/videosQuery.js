import { gql } from '@apollo/client';

function videosQuery({ route }) {

  return gql`
  {
    matchVideoCollection: videoCollection(
      order: views_DESC, 
      where: {
        AND: [
            // {match: {sys: {id: "${route.params.sys.id}"}}},
            {thumbnail_exists: true},
        ]
      }) {
      total
      items {
        ...videoFields_old
      }
    }
  }

  fragment videoFields_old on Video {
    sys {
      id
    }
    title
    thumbnail
    views
    viewsPerHour
    duration
    youtubeId
    publishDate
  }
  `
}

export default videosQuery;