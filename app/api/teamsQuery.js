import { gql } from '@apollo/client';

export const TeamsQuery = gql`
query($skip: Int, $limit: Int){
  teamCollection(
      order: name_ASC
      skip: $skip
      limit: $limit
    ) {
      items {
        ...teamFields
      }
    }
}
  fragment teamFields on Team {
    sys {
      id
    }
    name
    country
    countryId 
    logoUrl
    isClub
  }
 `;

export const SearchingTeamsQuery = (typedText) => (gql`
query($skip: Int, $limit: Int){
  teamCollection(
      order: name_ASC,
      skip: $skip,
      limit: $limit,
      where: {name_contains: "${typedText}" }
    ) {
      items {
        ...teamFields
      }
    }
}
  fragment teamFields on Team {
    sys {
      id
    }
    name
    country
    countryId 
    logoUrl
    isClub
  }
 `);
