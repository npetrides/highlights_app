import React from "react";
import { Platform, View } from "react-native";
import colors from "./colors";
import dimensions from "./dimensions";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export default {
  teamNameWithHeartIcon: {
    flexDirection: 'row',
  },
  spinnerView: { width: '100%', height: 40, alignItems: 'center' },
  colors,
  navStyles: {
    headerBackImage: () => <View style={{ paddingLeft: 11 }}><MaterialCommunityIcons name="chevron-left" size={30} color={colors.text} /></View>, // https://reactnavigation.org/docs/native-stack-navigator#headerbackimagesource
    headerStyle: {
      backgroundColor: "transparent",
    },
    gestureEnabled: true,
  },
  showHeader: {
    headerShown: true,
    headerTitle: false,
  },
  hideHeader: {
    headerShown: false
  },

  text: {
    color: colors.text,
    fontSize: 16,
    fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
  },
  container: {
    height: dimensions.window_height,
    width: dimensions.window_width,
    paddingLeft: 20,
    paddingRight: 20,
  },
  header: {
    flex: 1,
    flexDirection: "row",
  },

  // Matches Styles
  addIcon: {
    width: 50,
    height: 50,
    borderRadius: 50,
    margin: 20,
    marginBottom: 10,
    backgroundColor: colors.offBackground,
  },

  // Toast styles
  toast: {
    position: "absolute",
    marginTop: 40,
    width: "100%"
  },

  // App-wide styles
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: colors.text,
    paddingTop: 20,
    paddingBottom: 20,
  },
  titleWithNav: {
    fontSize: 24,
    fontWeight: "bold",
    color: colors.text,
    paddingBottom: 20,
    marginRight: 10
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  listItem: {
    backgroundColor: colors.background,
    overflow: "visible",
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 20,
    flexDirection: "row",
  },
  detailsContainer: {
    width: dimensions.details_width,
    padding: 10,
    paddingTop: 0,
    flexDirection: "row",
  },

  // Activity Indicator
  spinner: {
    // flexDirection: 'row',
    // justifyContent: 'center',
    // flex: 1,
    top: (dimensions.window_height * 0.075),
  },

  // Logo ListItem Styles
  logo: {
    container: {
      backgroundColor: colors.background,
      borderRadius: 0,
      width: dimensions.logo_height,
      height: dimensions.logo_height,
    },
    logoContainer: {
      width: dimensions.logo_height,
      height: dimensions.logo_height,
      resizeMode: 'contain',
    },
    title: {
      color: colors.text,
      fontWeight: "bold",
      fontSize: 14,
      position: "relative",
      marginTop: 10,
    },
    detailsContainer: {
      padding: 10,
      paddingTop: 0,
      flexDirection: "row",
    },
    favoriteIndicator: {
      marginTop: 12,
      marginLeft: 5,
    }
  },

  // TeamIcon Styles
  teamIcon: {
    container: {
      backgroundColor: colors.background,
      borderRadius: 0,
      sliderWidth: 100,
      width: 55,
      height: 55,
      margin: 10,
      marginBottom: 20,
    },
    title: {
      fontSize: 24,
      fontWeight: "bold",
      color: colors.text,
      paddingLeft: 20,
    },
    imageContainer: {
      width: 55,
      height: 55,
      margin: 10,
      resizeMode: 'contain',
    }
  },

  // Video ListItem Styles
  video: {
    detailsContainer: {
      width: dimensions.list_item_details_width,
      padding: 10,
      paddingTop: 0
    },
    image: {
      width: dimensions.image_width,
      height: dimensions.image_height,
      borderRadius: 5,
    },
    title: {
      color: colors.text,
      fontWeight: "bold",
    },
    subTitle: {
      color: colors.textAlt,
      fontWeight: "bold",
      fontSize: 14,
    },
    date: {
      color: colors.subText,
      fontWeight: "bold",
      fontSize: 12,
    },
    status: {
      paddingLeft: 4,
    }
  },
  // Video Details Styles
  videoDetails: {
    container: {
      height: dimensions.video_height,
    },
    detailsContainer: {
      paddingTop: 20,
      paddingLeft: 20,
    },
    image: {
      width: dimensions.video_width,
      height: dimensions.video_height,
      marginBottom: -dimensions.video_height
    },
    flatList: {
      top: 20,
      height: dimensions.other_videos_height,
    },
    header: {
      flexDirection: "row",
    },
    title: {
      fontSize: 20,
      fontWeight: "bold",
      color: colors.text,
      marginBottom: 10,
    },
    competition: {
      color: colors.textAlt,
      fontWeight: "bold",
      fontSize: 16,
    },
  },
  // Feed Styles
  feed: {
    title: {
      fontSize: 24,
      fontWeight: "bold",
      color: colors.text,
      paddingBottom: 20,
      paddingLeft: 20,
      paddingTop: 20,
    },
    container: {
      paddingTop: 20,
    },
    listItem: {
      paddingLeft: 20,
    },
  },
  // Carousel Styles
  carousel: {
    card: {
      width: dimensions.carousel_image_width,
    },
    favorite: {
      position: 'absolute',
      zIndex: 99999,
      left: dimensions.carousel_image_width - (dimensions.carousel_image_width * .09),
      top: dimensions.image_height * .025
    },
    detailsContainer: {
      padding: 10,
      paddingLeft: 0,
    },
    image: {
      width: dimensions.carousel_image_width,
      height: dimensions.carousel_image_height,
      borderRadius: 5,
    },
    subTitle: {
      color: colors.textAlt,
      fontWeight: "bold",
      fontSize: 14,
    },
    date: {
      color: colors.subText,
      fontWeight: "bold",
      fontSize: 12,
    },
    title: {
      color: colors.text,
      fontWeight: "bold",
    },
  },
};
