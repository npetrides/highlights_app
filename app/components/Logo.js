import React from "react";
import { View, TouchableWithoutFeedback } from "react-native";
import ExpoFastImage from 'expo-fast-image';
import Text from "./Text";
import styles from "../config/styles";

function Logo({ title, subTitle, imageUrl, onPress, favorite, uniqueId }) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.listItem}>
        <ExpoFastImage
          uri={imageUrl}
          cacheKey={uniqueId}
          style={styles.logo.logoContainer}
        />
        <View style={styles.logo.detailsContainer}>
          <Text style={styles.logo.title}>{title}{subTitle}</Text>
          <Text style={styles.logo.favoriteIndicator}>{favorite}</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

export default Logo;
