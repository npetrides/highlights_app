import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import MatchesScreen from "../screens/MatchesScreen";
import TeamMatchesScreen from "../screens/TeamMatchesScreen";
import MatchDetailsScreen from "../screens/MatchDetailsScreen";
import CompetitionMatchesScreen from "../screens/CompetitionMatchesScreen";
import styles from "../config/styles";
import TeamsScreen from "../screens/TeamsScreen";

const Stack = createStackNavigator();

const FeedNavigator = () => (
  <Stack.Navigator headerMode="screen" screenOptions={styles.navStyles}>
    <Stack.Screen name="Matches" component={MatchesScreen} options={styles.hideHeader}/>
    <Stack.Screen name="CompetitionMatches" component={CompetitionMatchesScreen} options={styles.showHeader}/>
    <Stack.Screen name="TeamMatches" component={TeamMatchesScreen} options={styles.showHeader}/>
    <Stack.Screen name="MatchDetails" component={MatchDetailsScreen} options={styles.showHeader}/>
    <Stack.Screen name="Teams" component={TeamsScreen} options={styles.showHeader}/>
  </Stack.Navigator>
);

export default FeedNavigator;
