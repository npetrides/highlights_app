import React, { useState, useEffect } from "react";
import { FlatList, View, SafeAreaView, ActivityIndicator } from "react-native";
import Logo from "../components/Logo";
import styles from "../config/styles";
import colors from "../config/colors";
import routes from "../navigation/routes";
import Text from "../components/Text";
import { useQuery } from '@apollo/client';
import CountryCompetitionsQuery from "../api/countryCompetitionsQuery";
import * as storage from "../api/asyncStorage";
import { useAsyncStorage } from '@react-native-async-storage/async-storage';

const wait = timeout => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};

function CompetitionsScreen({ navigation, route }) {
  const item = route.params;
  const CompetitionsQuery = CountryCompetitionsQuery(item.countryId);
  const { loading, error, data } = useQuery(CompetitionsQuery);
  const [value, setValue] = useState('value');
  const { getItem, setItem } = useAsyncStorage('favoriteLeagues');
  const [rerender, setRerender] = useState(false);

  const readItemFromStorage = async () => {
    const item = await getItem();
    setValue(item);
    setRerender(!rerender);
  };

  useEffect(() => {
    readItemFromStorage({});
  }, []);

  if (loading) return <View style={styles.spinner}><ActivityIndicator size="large" color={colors.text} /></View>;
  if (error) return <Text>Error :(</Text>;

  return (
    <SafeAreaView>
      <FlatList
        style={styles.container}
        // refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} tintColor={colors.text} />}
        ListHeaderComponent={
          <Text style={styles.titleWithNav}>{item.country}</Text>
        }
        data={data.competitionCollection.items}
        keyExtractor={(item, index) => String(item.sys.id + index)}
        renderItem={({ item }) => (
          <Logo
            uniqueId={item.name.replace(/[^\w]/g, "") + item.sys.id.toString()}
            title={item.name}
            imageUrl={item.logoUrl}
            thumbnailUrl={item.logoUrl}
            favorite={storage.favoriteIndicator(value, item.sys.id, item.sys.id)}
            onPress={() => navigation.navigate(routes.COMPETITION_MATCHES, {
              competitionName: item.name,
              competitionId: item.sys.id
            })}
          />
        )}
      />
    </SafeAreaView>
  );
}

export default CompetitionsScreen;