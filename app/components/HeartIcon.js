import React from "react"
import { TouchableOpacity } from "react-native"
import { MaterialCommunityIcons } from "@expo/vector-icons";

const HeartIcon = ({ onPress, color }) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <MaterialCommunityIcons name="heart" size={30} color={color} />
        </TouchableOpacity>
    )
}

export default HeartIcon;