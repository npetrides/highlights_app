import React, { useState } from "react";
import { FlatList, View, ActivityIndicator, SafeAreaView, Image } from "react-native";
import YoutubePlayer from "react-native-youtube-iframe";
import colors from "../config/colors";
import styles from "../config/styles";
import ListItem from "../components/ListItem";
import Text from "../components/Text";
import { useQuery, gql } from '@apollo/client';
import matchDetailsQuery from "../api/matchDetailsQuery";
import dimensions from "../config/dimensions";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import moment from "moment";
import { TouchableOpacity } from "react-native-gesture-handler";
import routes from "../navigation/routes";

function MatchDetailsScreen({ navigation, route }) {
  const item = route.params;

  const [video, updateVideo] = useState({
    youtubeId: item.primaryVideo.youtubeId,
  });

  const changeVideo = (youtubeId) => {
    updateVideo({
      youtubeId: youtubeId,
    })
  }

  const nowPlaying = (video, item) => {
    if (video === item) {
      return <MaterialCommunityIcons name="video" size={14} color={colors.textHighlight} />
    }
  }

  const DetailsQuery = matchDetailsQuery(item.sys.id);
  const { loading, error, data, refetch } = useQuery(DetailsQuery);
  if (loading) return <View style={styles.spinner}><ActivityIndicator size="large" color={colors.text} /></View>;
  if (error) return <Text>Error :(</Text>;

  // console.log(item)

  return (
    <SafeAreaView style={styles.videoDetails.container}>
      <Image
        style={styles.videoDetails.image}
        source={{
          uri: item.primaryVideo.thumbnail,
        }}
      />
      <View style={styles.videoDetails.video}>
        <YoutubePlayer
          width={dimensions.video_width}
          height={dimensions.video_height}
          webViewProps={{ allowsInlineMediaPlayback: true }}
          videoId={video.youtubeId}
          play={true}
          initialPlayerParams={{
            controls: true,
            modestbranding: 1,
          }}
        />
      </View>
      <View style={styles.videoDetails.detailsContainer}>
        <View style={styles.videoDetails.header}>
          <Text style={styles.videoDetails.title} onPress={() => navigation.push(routes.TEAM_MATCHES, { teamName: item.homeSide.name, teamId: item.homeSide.sys.id })}>{item.homeSide.name} vs&nbsp;</Text>
          <Text style={styles.videoDetails.title} onPress={() => navigation.push(routes.TEAM_MATCHES, { teamName: item.awaySide.name, teamId: item.awaySide.sys.id })}>{item.awaySide.name}</Text>
        </View>
        <Text style={styles.videoDetails.competition} onPress={() => navigation.push(routes.COMPETITION_MATCHES, { competitionName: item.competition.name, competitionId: item.competition.sys.id })}>{item.competition.name}</Text>
        <FlatList
          style={styles.videoDetails.flatList}
          ListHeaderComponent={<Text style={styles.videoDetails.title}>Match Videos</Text>}
          data={data.matchVideoCollection.items}
          keyExtractor={(item, index) => String(item.sys.id + index)}
          renderItem={({ item }) => (
            <ListItem
              uniqueId={item.title.replace(/[^\w]/g, "") + item.sys.id.toString()}
              title={item.title}
              subTitle={new Date(1000 * item.duration).toISOString().substr(11, 8).replace("00:", "")}
              date={moment(item.publishDate).fromNow()}
              status={nowPlaying(video.youtubeId, item.youtubeId)}
              imageUrl={item.thumbnail}
              videoId={item.youtubeId}
              thumbnailUrl={item.thumbnail}
              onPress={() => changeVideo(item.youtubeId)}
            />
          )}
        />
      </View>
    </SafeAreaView>
  );
}

export default MatchDetailsScreen;