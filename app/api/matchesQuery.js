// https://www.apollographql.com/docs/react/v2/integrations/react-native/
// https://www.youtube.com/watch?v=bDgm2NwUDRk

import { gql } from '@apollo/client';

const topLeagues = `[
  "ENGLAND: Premier League",
  "GERMANY: Bundesliga",
  "SPAIN: La Liga",
  "ITALY: Serie A",
  "FRANCE: Ligue 1"
]`

const topCountries = `[
  "CHAMPIONS LEAGUE"
]`

const popularTeams = `[
  "Manchester United",
  "Manchester City",
  "Chelsea",
  "Liverpool",
  "Arsenal",
  "Tottenham",
  "PSG",
  "Borussia Dortmund",
  "Bayern Munich",
  "Real Madrid",
  "Barcelona",
  "Atletico Madrid",
  "AC Milan",
  "Juventus",
  "Napoli",
  "Inter Milan"
]`

const leaguesTrendRate = 5000
const countriesTrendRate = 100

const MatchesQuery = (favoriteTeams, favoriteLeagues) => (
  gql`
  query($skip: Int, $limit: Int){
    popularTeamsCollection: teamCollection(
      where: {
        sys: {id_in: ${favoriteTeams}}
      }) {
      total
      items {
        name
        logoUrl
        sys {
          id
        }
      }
    },
    popularMatchCollection: matchCollection(
      limit: 25, 
      order: [
        matchDate_DESC,
      ],
      where: {
        AND: [
          {competition_exists: true},
          {primaryVideo: {thumbnail_exists: true}},
          {homeSide: {sys: {id_exists: true}}},
          {awaySide: {sys: {id_exists: true}}},
          {OR: [
              {homeSide: {sys: {id_in: ${favoriteTeams}}}}, 
              {awaySide: {sys: {id_in: ${favoriteTeams}}}},
              {competition: {sys: {id_in: ${favoriteLeagues}}}},
              {AND: [
                {competition: {name_in: ${topLeagues}}},
                {primaryVideo: {viewsPerHour_gt: ${leaguesTrendRate}}}
              ]},
              {AND: [
                {competition: {country_in: ${topCountries}}},
                {competition: {name_not_contains: "Women"}},
                {primaryVideo: {viewsPerHour_gt: ${countriesTrendRate}}}
                ]},
              ]},
            ]
       }) {
        total
        items {
          ...matchFields
        }
      },
    recentMatchCollection: matchCollection(
      skip: $skip,
      limit: $limit ,
      order: [
        matchDate_DESC,
      ],
      where: {
      AND: [
        {competition_exists: true},
        {primaryVideo: {thumbnail_exists: true}},
        {homeSide: {sys: {id_exists: true}}},
        {awaySide: {sys: {id_exists: true}}},
        {OR: [
            {homeSide: {sys: {id_not_in: ${favoriteTeams}}}}, 
            {awaySide: {sys: {id_not_in: ${favoriteTeams}}}},
            {competition: {sys: {id_not_in: ${favoriteLeagues}}}},
            {AND: [
              {competition: {name_in: ${topLeagues}}},
              {primaryVideo: {viewsPerHour_lte: ${leaguesTrendRate}}}
            ]},
            {AND: [
              {competition: {country_in: ${topCountries}}},
              {primaryVideo: {viewsPerHour_lte: ${countriesTrendRate}}}
              ]},
            ]},
          ]
     }) {
      total
      items {
        ...matchFields
      }
    }
  }

  fragment matchFields on Match {
    sys {
      id
    }
    title
    matchDate
    side1
    side2
    homeSide {
      name
      sys {
        id
      }
    }
    awaySide {
      name
      sys {
        id
      }
    }
    competition {
      name
      country
      sys {
        id
      }
    }
    primaryVideo {
      thumbnail
      views
      viewsPerHour
      duration
      youtubeId
    }
  }
  `
);

export default MatchesQuery;