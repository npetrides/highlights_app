// https://www.apollographql.com/docs/react/v2/integrations/react-native/
// https://www.youtube.com/watch?v=bDgm2NwUDRk

import { gql } from '@apollo/client';

const MatchDetailsQuery = (matchId) => (
  gql`
  {
    matchVideoCollection: videoCollection(
      order: views_DESC, 
      where: {
        AND: [
            {match: {sys: {id: "${matchId}"}}},
            {thumbnail_exists: true},
        ]
      }) {
      total
      items {
        ...videoFields
      }
    }
  }

  fragment videoFields on Video {
    sys {
      id
    }
    title
    thumbnail
    views
    viewsPerHour
    duration
    youtubeId
    publishDate
  }
  `
);

export default MatchDetailsQuery;
