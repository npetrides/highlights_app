// https://www.apollographql.com/docs/react/v2/integrations/react-native/
// https://www.youtube.com/watch?v=bDgm2NwUDRk

import { gql } from '@apollo/client';

const TeamMatchesQuery = (teamName) => (
  gql`
  query($skip: Int, $limit: Int){
    recentMatchCollection: matchCollection(
      skip: $skip,
      limit: $limit, 
      order: matchDate_DESC, 
      where: {
        AND: [
          {primaryVideo: {thumbnail_exists: true}},
          {
            OR: [
							{side1: "${teamName}"},
              {side2: "${teamName}"}
            ]
          }
        ]
        }) {
      total
      items {
        ...teamMatchesMatchFields
      }
    }
  }

  fragment teamMatchesMatchFields on Match {
    sys {
      id
    }
    title
    side1
    side2
    homeSide {
      name
      sys {
        id
      }
    }
    awaySide {
      name
      sys {
        id
      }
    }
    matchDate
    competition {
      name
    }
    primaryVideo {
      thumbnail
      views
      viewsPerHour
      duration
      youtubeId
    }
  }
  `
);

export default TeamMatchesQuery;
