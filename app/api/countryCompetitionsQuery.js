// https://www.apollographql.com/docs/react/v2/integrations/react-native/
// https://www.youtube.com/watch?v=bDgm2NwUDRk

import { gql } from '@apollo/client';

const CountryCompetitionsQuery = (country) => (
  gql`
  {
    competitionCollection(
      order: name_ASC
      where: {countryId:  ${country}},
    ) {
      items {
        ...competitionFields
      }
    }
  }

  fragment competitionFields on Competition {
    sys {
      id
    }
    name
    country
    logoUrl
  }
  `
);

export default CountryCompetitionsQuery;
