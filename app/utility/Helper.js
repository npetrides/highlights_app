import React from "react";

export const ShowToast = (message) => {
    toast.show(message, {
        type: "normal", // | success | warning | danger | custom",
        placement: "top", // top | bottom,
        duration: 1000,
        animationType: "slide-in", // slide-in | zoom-in",
    })
} 