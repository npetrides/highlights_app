import React, {useState, useEffect} from "react";
import { FlatList, ScrollView, View, SafeAreaView,  ActivityIndicator, TouchableWithoutFeedback } from "react-native";
import Card from "../components/Card";
import ListItem from "../components/ListItem";
import colors from "../config/colors";
import styles from "../config/styles";
import * as storage from "../api/asyncStorage";
import dimensions from "../config/dimensions";
import routes from "../navigation/routes";
import Text from "../components/Text";
import Carousel from 'react-native-snap-carousel';
import { useQuery } from '@apollo/client';
import moment from "moment";
import MatchesQuery from "../api/matchesQuery";
import AsyncStorage from '@react-native-async-storage/async-storage';
import TeamIcon from "../components/TeamIcon";
import { MaterialCommunityIcons } from "@expo/vector-icons";

let isCheck = false;
const PAGE_SIZE = 10;

function MatchesScreen({ navigation }) {

  const [page, setPage] = useState(1);
  const [matches, setMatches] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => { // The screen is focused, call any action and update data https://reactnavigation.org/docs/navigation-events/
      getFavorites();
    });
    return unsubscribe; // Return the function to unsubscribe from the event so it gets removed on unmount
  }, [navigation]);

  const [favoriteTeams, setFavoriteTeams] = useState('favoriteTeams');
  const [favoriteLeagues, setFavoriteLeagues] = useState('favoriteLeagues');
  
  const getFavorites = async () => {
    await AsyncStorage.multiGet(['favoriteTeams', 'favoriteLeagues']).then(response => {
      setFavoriteTeams(response[0][1]);
      setFavoriteLeagues(response[1][1]);
    })
  }
  
  useEffect(() => {
    getFavorites({});
  }, []);

  const allMatchesQuery = MatchesQuery(favoriteTeams || '[]', favoriteLeagues || '[]');
  const { loading, error, data } = useQuery(allMatchesQuery, {
    variables: {
      skip: page === 1 ? 0 : page * PAGE_SIZE,
      limit: PAGE_SIZE,
    },
    fetchPolicy: "network-only"// Doesn't check cache before making a network request https://www.apollographql.com/docs/react/data/queries/#refetching
  });

  useEffect(() => {
    data && setMatches([...matches, ...data.recentMatchCollection.items])
  }, [data]);

  const getMoreData = () => {
    setIsLoading(true);
    setTimeout(() => {
      setPage(prev => prev + 1)
      setIsLoading(false);
    }, 2000);
  };

    
  if (loading && !isCheck) return isCheck = true;
  if (error) return <></>;

  const items = data && data.recentMatchCollection.items;
  
  function favoritesLength() {
    if (favoriteTeams !== null) {
      return favoriteTeams.length
    } else {
      return 0
    }
  }


  var favoritesHeader = (favoritesLength() < 3) ? 
    <TouchableWithoutFeedback onPress={() => navigation.navigate(routes.TEAMS)}>
      <View>
        <View style={styles.addIcon}>
          <MaterialCommunityIcons name="plus" size={26} color={colors.text} style={{padding: 12}} />
        </View>
      </View>
    </TouchableWithoutFeedback>
    : <View>
    <Carousel
      data={data ? data.popularTeamsCollection.items : []}
      keyExtractor={(item, index) => String(item.sys.id + index)}
      useScrollView={true}
      sliderWidth={dimensions.slider_width}
      itemWidth={styles.teamIcon.container.width + styles.teamIcon.container.margin}
      layout={'default'}
      activeSlideAlignment={'start'}
      marginLeft={0}
      paddingLeft={10}
      renderItem={({ item }) => (
        <TeamIcon
          uniqueId={item.name.replace(/[^\w]/g, "")+item.sys.id.toString()}
          imageUrl={item.logoUrl}
          onPress={() => navigation.push(routes.TEAM_MATCHES, {
            teamName: item.name, 
            teamId: item.sys.id.toString()
            }
          )}
        />
      )}
    />  
  </View>;
  
  return (
    <SafeAreaView>
      <FlatList 
        style={styles.feed.container}
        onEndReachedThreshold={0.5}
        onEndReached={getMoreData}
        ListHeaderComponent={
          <ScrollView>
            <Text style={styles.teamIcon.title}>Favorites</Text>
            {favoritesHeader}
            <Text style={styles.feed.title}>Trending</Text>
            <Carousel
              data={data ? data.popularMatchCollection.items : []}
              keyExtractor={(item, index) => String(item.sys.id + index)}
              useScrollView={true}
              sliderWidth={dimensions.slider_width}
              itemWidth={dimensions.card_width}
              layout={'default'}
              activeSlideAlignment={'start'}
              marginLeft={0}
              paddingLeft={20}
              renderItem={({ item }) => (
                <Card
                  uniqueId={item.title.replace(/[^\w]/g, "") + `${item.homeSide.sys.id.toString()}${item.awaySide.sys.id.toString()}`}
                  title={item.title}
                  favoriteIndicator={storage.favoriteIndicator(favoriteTeams || '[]', item.homeSide.sys.id.toString(), item.awaySide.sys.id.toString())}
                  subTitle={item.competition.name}
                  date={moment(item.matchDate).fromNow()}
                  imageUrl={item.primaryVideo.thumbnail}
                  videoId={item.primaryVideo.youtubeId}
                  thumbnailUrl={item.primaryVideo.thumbnail}
                  onPress={() => navigation.navigate(routes.MATCH_DETAILS, item)}
                />
              )}
            />  
            <Text style={styles.feed.title}>Recent</Text>
          </ScrollView>
        }
        ListFooterComponent={
          <View style={styles.spinnerView}>
            {isLoading && <ActivityIndicator size="large" color={colors.text} />}
          </View>
        }
        data={matches.length === 0 ? items : matches}
        keyExtractor={(item, index) => String(item.sys.id + index)}
        renderItem={({ item }) => (
          <View style={styles.feed.listItem}>
          <ListItem 
            uniqueId={item.title.replace(/[^\w]/g, "") + `${item.homeSide.sys.id.toString()}${item.awaySide.sys.id.toString()}`}
            title={item.title}
            date={moment(item.matchDate).fromNow()}
            subTitle={item.competition.name}
            imageUrl={item.primaryVideo.thumbnail}
            thumbnailUrl={item.primaryVideo.thumbnail}
            videoId={item.primaryVideo.youtubeId}
            onPress={() => navigation.navigate(routes.MATCH_DETAILS, item)}
          />
          </View>
        )}
      />
    </SafeAreaView>
  );
}

export default MatchesScreen;





// const [value, setValue] = useState('value');
//   const [favorites, setFavorites] = useState('favorites');
//   const [favoriteTeams, setFavoriteTeams] = useState('favoriteTeams');
//   const [favoriteLeagues, setFavoriteLeagues] = useState('favoriteLeagues');
//   const { getItem, setItem } = useAsyncStorage('favoriteTeams');
//   // const [rerender, setRerender] = useState(false);
  
//   const readItemFromStorage = async () => {
//     const item = await getItem();
//     setValue(item);
//     // setRerender(!rerender);
//   };

//   const getFavorites = async () => {
//     const favorites = await AsyncStorage.multiGet(['favoriteTeams', 'favoriteLeagues', 'lalalalala']).then(response => {
//       // console.log(response[0][0]) // Key1
//       // console.log(response[0][1]) // Value1
//       // console.log(response[1][0]) // Key2
//       // console.log(response[1][1]) // Value2
//       // console.log(response[2][0]) // Key3
//       // console.log(response[2][1]) // Value3
//       setFavoriteTeams(response[0][1]);
//       setFavoriteLeagues(response[1][1]);
//   })
//     // setFavorites(favorites);
//     // console.log(favorites)
//     // console.log(favorites[2][1])
//   }
  
//   useEffect(() => {
//     readItemFromStorage();
//     getFavorites();
//   }, []);

//   // const formatQueryList = (list) => {
//   //   if (list === null) {
//   //     return '[]';
//   //   }
//   //   else {
//   //     return JSON.parse(JSON.stringify(list));
//   //   }    
//   // };

//   console.log(value)
//   console.log(favoriteTeams)
//   console.log(favoriteLeagues || '[]')

//   // console.log(formatQueryList(favorites[0][1]))
//   // console.log(formatQueryList(favorites[1][1]))
//   // console.log(formatQueryList(favorites[2][1]))


  
//   // console.log("Value " + (typeof value) + " " + value)
//   // console.log("Value " + (typeof favoriteTeams) + " " + favoriteTeams)
//   // console.log("Value " + (typeof favoriteLeagues) + " " + favoriteLeagues)

//   // const allMatchesQuery = MatchesQuery(
//   //   formatQueryList(favorites[0][1]), // Favorite Teams
//   //   formatQueryList(favorites[1][1]) // Favorite Leagues
//   //   );

//   const allMatchesQuery = MatchesQuery(favoriteTeams || '[]', favoriteLeagues || '[]');