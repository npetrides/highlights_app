import { gql } from '@apollo/client';

export const CountriesQuery = gql`
  query($skip: Int, $limit: Int){
    competitionCollection(
      order: name_ASC
      skip: $skip
      limit: $limit
    ) {
      items {
        ...countryFields
      }
    }
  }

  fragment countryFields on Competition {
    sys {
      id
    }
    name
    country
    countryId
    logoUrl
  }
  `;


export const SearchingCountriesQuery = (typedText) => (gql`
  query($skip: Int, $limit: Int){
    competitionCollection(
      order: name_ASC,
      skip: $skip,
      limit: $limit,
      where: {name_contains: "${typedText}" }
    ) {
      items {
        ...countryFields
      }
    }
  }

  fragment countryFields on Competition {
    sys {
      id
    }
    name
    country
    countryId
    logoUrl
  }
  `);