import { Dimensions } from "react-native";

// Window
const window = Dimensions.get("window");
const window_height = window.height
const window_width = window.width

// Videos
const slider_width = window.width
const card_width = slider_width*(2/3)

// Video Details
const video_width = window.width
const video_height = video_width*(9/16)-1
const other_videos_height = window.height - video_height

// Competitions
const logo_width = (window.width - 40)*(1/3)
const logo_height = logo_width*(5/16)
const details_width = (window.width - logo_width)

// Video List Item
const image_width = (window.width - 40)*(1/3)
const image_height = image_width*(9/16)
const list_item_details_width = (window.width - image_width)

// Video Carousel
const carousel_image_width = window.width*(2/3)
const carousel_image_height = carousel_image_width*(9/16)

export default {
  window_height: window_height,
  window_width: window_width,
  slider_width: slider_width,
  card_width: card_width,
  logo_width: logo_width,
  video_width: window.width,
  video_height: video_height,
  other_videos_height: other_videos_height,
  logo_width: logo_width,
  logo_height: logo_height,
  details_width: details_width,
  image_width: image_width,
  image_height: image_height,
  list_item_details_width: list_item_details_width,
  carousel_image_height: carousel_image_height,
  carousel_image_width: carousel_image_width,
};