import React, { useEffect, useState } from "react";
import { FlatList, ScrollView, SafeAreaView, View, ActivityIndicator } from "react-native";
import Logo from "../components/Logo";
import colors from "../config/colors";
import styles from "../config/styles";
import routes from "../navigation/routes";
import Text from "../components/Text";
import { useQuery } from '@apollo/client';
import { CountriesQuery, SearchingCountriesQuery } from "../api/countriesQuery";
import { SearchBar } from 'react-native-elements';
import 'react-native-gesture-handler';

let isCheck = false;
const PAGE_SIZE = 100;

function CountriesScreen({ navigation }) {
  const [query, setQuery] = useState("");
  const [countries, setCountries] = useState([]);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [isQueryEmpty, setIsQueryEmpty] = useState(false);


  const SearchQuery = SearchingCountriesQuery(query);
  const { loading, error, data } = useQuery(!isQueryEmpty ? CountriesQuery : SearchQuery,
    !isQueryEmpty && {
      variables: {
        skip: page === 1 ? 0 : page * PAGE_SIZE,
        limit: PAGE_SIZE,
      }
    });

  useEffect(() => {
    data && setCountries([...countries, ...data.competitionCollection.items.filter((v, i, a) => a.findIndex(t => (t.country === v.country)) === i)]);
    isQueryEmpty && setCountries([])
  }, [data]);

  const getMoreData = () => {
    setIsLoading(true);
    setTimeout(() => {
      setPage(prev => prev + 1)
      setIsLoading(false);
    }, 2000);
  };

  const onChangeQueryText = (text) => {
    if (text !== "") {
      setQuery(text);
      setIsQueryEmpty(true);
    } else {
      setQuery("");
      setIsQueryEmpty(false);
      setPage(1);
    }
  }

  if (loading && !isCheck) return isCheck = true;
  if (error) return <></>;

  const items = data && data.competitionCollection.items.filter((v, i, a) => a.findIndex(t => (t.country === v.country)) === i);

  const filteredItems = query === "" ? countries.length === 0 ? items : countries : items;

  return (
    <SafeAreaView>
      <FlatList
        style={styles.container}
        onEndReachedThreshold={0.5}
        onEndReached={getMoreData}
        ListHeaderComponent={
          <ScrollView>
            <Text style={styles.title}>Countries</Text>
            <SearchBar
              containerStyle={{
                backgroundColor: colors.background,
                borderWidth: 1,
                borderRadius: 10,
                marginBottom: 20,
                borderColor: colors.text,
                borderTopColor: colors.text,
                borderBottomColor: colors.text,
              }}
              inputContainerStyle={{
                backgroundColor: colors.background,
                borderWidth: 0
              }}
              leftIconContainerStyle={{
                backgroundColor: colors.background,
                borderWidth: 0
              }}
              inputStyle={{
                backgroundColor: colors.background,
                color: colors.text,
              }}
              placeholderTextColor={colors.text}
              onChangeText={text => onChangeQueryText(text)}
              value={query}
            />
          </ScrollView>
        }
        ListFooterComponent={
          <View style={styles.spinnerView}>
            {isLoading && <ActivityIndicator size="large" color={colors.text} />}
          </View>
        }
        data={filteredItems}
        keyExtractor={(item, index) => String(item.sys.id + index)}
        renderItem={({ item }) => (
          <Logo
            uniqueId={item.country.replace(/[^\w]/g, "") + item.sys.id.toString()}
            style={styles.flatList}
            title={item.country}
            imageUrl={item.logoUrl}
            thumbnailUrl={item.logoUrl}
            onPress={() => navigation.navigate(routes.COMPETITIONS, item)}
          />
        )}
      />
    </SafeAreaView>
  );
}

export default CountriesScreen;