import React from "react";
import { View, TouchableWithoutFeedback } from "react-native";
import { Image } from "react-native-expo-image-cache";
import ExpoFastImage from 'expo-fast-image';
import Text from "./Text";
import styles from "../config/styles";

function ListItem({ title, subTitle, date, status, imageUrl, thumbnailUrl, onPress, uniqueId }) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.listItem}>
        <ExpoFastImage
          uri={imageUrl}
          cacheKey={uniqueId}
          style={styles.video.image}
        />
        <View style={styles.video.detailsContainer}>
          <Text style={styles.video.title} numberOfLines={2}>{title}</Text>
          <Text style={styles.video.subTitle} numberOfLines={1}>
            {subTitle}
            <Text>
              <View style={styles.video.status}>{status}</View>
            </Text>
          </Text>
          <Text style={styles.video.date} numberOfLines={1}>{date}</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

export default ListItem;
