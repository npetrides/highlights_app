import React from "react";
import { View, TouchableWithoutFeedback } from "react-native";
import ExpoFastImage from 'expo-fast-image';
import styles from "../config/styles";
import Text from "./Text";

function Card({ title, subTitle, date, imageUrl, onPress, thumbnailUrl, favoriteIndicator, uniqueId }) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.carousel.card}>
        <View style={styles.carousel.favorite}>{favoriteIndicator}</View>
        <ExpoFastImage
          uri={imageUrl}
          cacheKey={`tren_${uniqueId}`}
          style={styles.carousel.image}
        />
        <View style={styles.carousel.detailsContainer}>
          <Text style={styles.carousel.title} numberOfLines={1}>
            {title}
          </Text>
          <Text style={styles.carousel.subTitle} numberOfLines={1}>
            {subTitle}
          </Text>
          <Text style={styles.carousel.date} numberOfLines={1}>
            {date}
          </Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

export default Card;
