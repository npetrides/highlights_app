import React from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import colors from "../config/colors";

function AccountScreen({ navigation }) {
return (
    <View style={styles.container}>
      <Image style={styles.logo} source={require("../assets/favicon.jpg")} />
      <Text style={styles.title}>Built by @nicholaspetrides</Text>
    </View>
);
}

const styles = StyleSheet.create({
logo: {
  width: 50,
  height: 50,
  borderRadius: 50,
},
container: {
  justifyContent: 'center',
  alignItems: 'center',
  flex:1
},
title: {
  fontSize: 14,
  fontWeight: "bold",
  color: colors.text,
  marginTop: 18,
},
});

export default AccountScreen;
