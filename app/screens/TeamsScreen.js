import React, { useState, useEffect } from "react";
import { ActivityIndicator, FlatList, ScrollView, SafeAreaView, View } from "react-native";
import Logo from "../components/Logo";
import colors from "../config/colors";
import styles from "../config/styles";
import routes from "../navigation/routes";
import Text from "../components/Text";
import { useQuery } from '@apollo/client';
import { SearchingTeamsQuery, TeamsQuery } from "../api/teamsQuery";
import { SearchBar } from 'react-native-elements';
import 'react-native-gesture-handler';
import * as storage from "../api/asyncStorage";
import { useAsyncStorage } from '@react-native-async-storage/async-storage';

let isCheck = false;
const PAGE_SIZE = 10;

function TeamsScreen({ navigation }) {
  const [query, setQuery] = useState("");
  const [value, setValue] = useState('value');
  const { getItem, setItem } = useAsyncStorage('favoriteTeams');
  const [rerender, setRerender] = useState(0);
  const [page, setPage] = useState(1);
  const [team, setTeam] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isQueryEmpty, setIsQueryEmpty] = useState(false);

  const readItemFromStorage = async () => {
    const item = await getItem();
    setValue(item);
    setRerender(!rerender);
  };

  useEffect(() => {
    readItemFromStorage({});
  }, []);

  const SearchQuery = SearchingTeamsQuery(query);

  const { loading, error, data } = useQuery(!isQueryEmpty ? TeamsQuery : SearchQuery,
    !isQueryEmpty && {
      variables: {
        skip: page === 1 ? 0 : page * PAGE_SIZE,
        limit: PAGE_SIZE,
      }
    }
  );

  useEffect(() => {
    data && setTeam([...team, ...data.teamCollection.items])
    isQueryEmpty && setTeam([])
  }, [data]);

  const getMoreData = () => {
    setIsLoading(true);
    setTimeout(() => {
      setPage(prev => prev + 1)
      setIsLoading(false);
    }, 2000);
  };

  const subTitle = (isClub, country) => {
    if (isClub === true) {
      return `, ${country}`
    }
    else {
      return ` National Team`
    }
  }

  const onChangeQueryText = (text) => {
    if (text !== "") {
      setQuery(text);
      setIsQueryEmpty(true);
    } else {
      setQuery("");
      setIsQueryEmpty(false);
      setPage(1);
    }
  }

  if (loading && !isCheck) return isCheck = true;
  if (error) return <></>;
  const items = data && data.teamCollection.items;

  const filteredItems = query === "" ? team.length === 0 ? items : team : items

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList
        style={styles.container}
        onEndReachedThreshold={0.5}
        onEndReached={getMoreData}
        ListHeaderComponent={
          <ScrollView>
            <Text style={styles.title}>Teams</Text>
            <SearchBar
              containerStyle={{
                backgroundColor: colors.background,
                borderWidth: 1,
                borderRadius: 10,
                marginBottom: 20,
                borderColor: colors.text,
                borderTopColor: colors.text,
                borderBottomColor: colors.text,
              }}
              inputContainerStyle={{
                backgroundColor: colors.background,
                borderWidth: 0
              }}
              leftIconContainerStyle={{
                backgroundColor: colors.background,
                borderWidth: 0
              }}
              inputStyle={{
                backgroundColor: colors.background,
                color: colors.text,
              }}
              placeholderTextColor={colors.text}
              onChangeText={text => onChangeQueryText(text)}
              value={query}
            />

          </ScrollView>
        }
        ListFooterComponent={
          <View style={styles.spinnerView}>
            {isLoading && <ActivityIndicator size="large" color={colors.text} />}
          </View>
        }
        data={filteredItems}
        keyExtractor={(item, index) => String(item.sys.id + index)}
        renderItem={({ item }) => {
          return (
            <Logo
              uniqueId={item.name.replace(/[^\w]/g, "") + item.sys.id.toString()}
              title={item.name}
              subTitle={subTitle(item.isClub, item.country)}
              favorite={storage.favoriteIndicator(value, item.sys.id, item.sys.id)}
              imageUrl={item.logoUrl}
              thumbnailUrl={item.logoUrl}
              onPress={() => navigation.navigate(routes.TEAM_MATCHES, {
                teamName: item.name,
                teamId: item.sys.id
              }
              )}
            />
          )
        }}
      />
    </SafeAreaView>
  );
}

export default TeamsScreen;