import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { StatusBar } from 'expo-status-bar';
import navigationTheme from "./app/navigation/navigationTheme";
import AppNavigator from "./app/navigation/AppNavigator";
import OfflineNotice from "./app/components/OfflineNotice";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import Toast from "react-native-toast-notifications";
import styles from "./app/config/styles";

const client = new ApolloClient({
  uri: "https://graphql.contentful.com/content/v1/spaces/sxvdu4a1fwqr",
  credentials: "same-origin",
  headers: {
    Authorization: `Bearer C5cSPy4bJPBa4TvOmxtwh_RBl-Yifz8UUhG2R_qahdU`
  },
  cache: new InMemoryCache()
});

export default function App() {
  return (
    <ApolloProvider client={client}>
      <StatusBar style="auto" />
      <Toast ref={(ref) => global['toast'] = ref} style={styles.toast}/>
      <OfflineNotice />
      <NavigationContainer theme={navigationTheme}>
        <AppNavigator />
      </NavigationContainer>
    </ApolloProvider>
  );
}
