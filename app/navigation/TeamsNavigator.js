import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import TeamsScreen from "../screens/TeamsScreen";
import TeamMatchesScreen from "../screens/TeamMatchesScreen";
import MatchDetailsScreen from "../screens/MatchDetailsScreen";
import styles from "../config/styles";

const Stack = createStackNavigator();

const TeamsNavigator = () => (
  <Stack.Navigator headerMode="screen" screenOptions={styles.navStyles}>
    <Stack.Screen name="Teams" component={TeamsScreen} options={styles.hideHeader}/>
    <Stack.Screen name="TeamMatches" component={TeamMatchesScreen} options={styles.showHeader}/>
    <Stack.Screen name="MatchDetails" component={MatchDetailsScreen} options={styles.showHeader}/>
  </Stack.Navigator>
);

export default TeamsNavigator;
