import { Appearance } from 'react-native';

export default {

  danger: "#ff5252",
  textAlt: "#4ecdc4",
  subText: "#6e6969",
  textHighlight: "#fc5c65",
  text: Appearance.getColorScheme() === "light" ? "#264653" : "#D0CFD2",
  background: Appearance.getColorScheme() === "light" ? "#fff" : "#0D0D0D",
  offBackground: Appearance.getColorScheme() === "light" ? "#e8e8e8" : "#333333",
};

// https://coolors.co/264653-4ecdc4-fc5c65
// 264653-2a9d8f-e9c46a-f4a261-e76f51