// https://www.apollographql.com/docs/react/v2/integrations/react-native/
// https://www.youtube.com/watch?v=bDgm2NwUDRk

import { gql } from '@apollo/client';

const CompetitionMatchesQuery = (competitionName) => (
  gql`
  query($skip: Int, $limit: Int){
    recentMatchCollection: matchCollection(
      skip: $skip,
      limit: $limit,
      order: matchDate_DESC, 
      where: {
        AND: [
          {competition: {sys: {id: "${competitionName}"}}},
          {primaryVideo: {thumbnail_exists: true}},
        ]
        }) {
      total
      items {
        ...competitionMatchesMatchFields
      }
    }
  }

  fragment competitionMatchesMatchFields on Match {
    sys {
      id
    }
    title
    matchDate
    side1
    side2
    homeSide {
      name
      sys {
        id
      }
    }
    awaySide {
      name
      sys {
        id
      }
    }
    competition {
      name
    }
    primaryVideo {
      thumbnail
      views
      viewsPerHour
      duration
      youtubeId
    }
  }
  `
);

export default CompetitionMatchesQuery;
